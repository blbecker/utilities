#!/usr/bin/env bash

BLACK='\033[0;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
BROWN='\033[0;33m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHTGRAY='\033[0;37m'
DARKGRAY='\033[1;30m'
LIGHTRED='\033[1;31m'
LIGHTGREEN='\033[1;32m'
LIGHTBLUE='\033[1;34m'
YELLOW='\033[1;33m'
LIGHTPURPLE='\033[1;35m'
LIGHTCYAN='\033[1;36m'
WHITE='\033[1;37m'
NC='\033[0m'

declare -a COLORARRAY=(${BLACK} ${RED} ${GREEN} ${BROWN} ${ORANGE} ${BLUE} ${PURPLE} ${CYAN} ${LIGHTGRAY} ${DARKGRAY} ${LIGHTRED} ${LIGHTGREEN} ${LIGHTBLUE} ${YELLOW} ${LIGHTPURPLE} ${LIGHTCYAN} ${WHITE} ${NC})

print_rainbow() {
  for (( i = 30; i < 38; i++ )); do echo -e "\033[0;"$i"m Normal: (0;$i); \033[1;"$i"m Light: (1;$i)"; done
}

print_colors() {
  for i in $(seq 1 17); do
    color=${COLORARRAY[${i}]}
    echo -e "${color}I am color ${i}${NC}"
  done
}