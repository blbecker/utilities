#!/usr/bin/env bash

. "$(dirname "${BASH_SOURCE[0]}")/resources/colors.sh"

CURRENT_DIR=$(pwd)

print_unpushed_commits(){
    unpushedCommits="${1}"
    IFS_BAK=$IFS
    IFS=$'\n'

    for line in ${unpushedCommits}; do
        messageBody=$(echo "${line}" | awk -F' ' '{ st = index($0," "); print substr($0,st+1)}')
        (( ${#messageBody} > 100 )) && messageBody="${messageBody:0:97}..."
        printf "${LIGHTBLUE}+${NC} %s\n" "${messageBody}"
    done

    IFS=$IFS_BAK
    IFS_BAK=
}

for directory in "${CURRENT_DIR}"/* ; do
    if [ -d "${directory}" ]; then
        cd  "${directory}"
        dirName="$(basename "${directory}")"
        if [ -d ".git" ]; then
            branch="$(git symbolic-ref --short -q HEAD)"
            changes=$(git status -s | wc -l | xargs)
            unpushedCommits="$(git cherry -v)"
            unpushedCommitCount=$(git cherry -v | wc -l | xargs)
            if [ "${changes}" -ne 0 ] && [ "${unpushedCommitCount}" -ne 0 ]; then
                printf "${RED}%-30.30s | %s change(s) | %s Unpushed Commits${NC}\n" ${dirName} ${changes} ${unpushedCommitCount}
                git status -s
                print_unpushed_commits "${unpushedCommits}"
                echo -e ""
            elif [ "${changes}" -eq 0 ] && [ "${unpushedCommitCount}" -ne 0 ]; then
                printf "${LIGHTBLUE}%-30.30s | Up to date   | %s Unpushed Commits${NC}\n" ${dirName} ${unpushedCommitCount}

                print_unpushed_commits "${unpushedCommits}"

            elif [ "${changes}" -ne 0 ] && [ "${unpushedCommitCount}" -eq 0 ]; then
                printf "${YELLOW}%-30.30s | %s change(s)  | %s Unpushed Commits${NC}\n" ${dirName} ${changes} ${unpushedCommitCount}
                git status -s
            else
                printf "${GREEN}%-30.30s | Up to date   | All Commits Pushed${NC}\n" ${dirName} ${unPushedCommitCount}
            fi
        fi
        cd ..
    fi
done

cd "${CURRENT_DIR}"

